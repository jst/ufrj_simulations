%% itaVA simple example code

% Start run_VAServer_multichannel_array.bat!!

% Create itaVA
va = itaVA;

% Connect to VA application (start the application first)
va.connect( 'localhost' )

% Reset VA to clear the scene
va.reset()

% Control output gain
va.setOutputGain( .25 )

% Add the current absolute folder path to VA application
va.addSearchPath( pwd ); 

% Create a signal source and start playback
%ita_write_wav( ita_demosound, 'ita_demosound.wav', 'overwrite' );
%X = va.createAudiofileSignalSource( 'Audiofiles/lang_short.wav' );
X = va.createAudiofileSignalSource( 'audios/teste1_carro1_v30.wav' );
va.setAudiofileSignalSourcePlaybackAction( X, 'play' )
va.setAudiofileSignalSourceIsLooping( X, true );

% Create a virtual sound source and set a position
car = va.createSoundSource( 'car' );
%va.setSoundSourcePosition( S, [0 1.7 -2] )

% Connect the signal source to the virtual sound source
va.setSoundSourceSignalSource( car, X )

L1 = va.createListener( 'array_mic_1' );
va.setListenerPosition( L, [ 0.2 1.26 0 ] )
L2 = va.createListener( 'array_mic_2' );
va.setListenerPosition( L, [ 0 1.26 0 ] )
L3 = va.createListener( 'array_mic_3' );
va.setListenerPosition( L, [ -0.2 1.26 0 ] )
L4 = va.createListener( 'array_mic_4' );
va.setListenerPosition( L, [ 0 1.06 0 ] )

% Set the listener as the active one
%va.setActiveListener( L )

% Now close connection
%va.disconnect()

% VA virtual scene is still active now ...

% Explore itaVA class ...
%doc itaVA





%% Loop to move car

vehicle_velocity = 10; % m/s

% Spatial frames
T = 1/60;
nframes = 60 * 10;
disp( [ 'Starting update loop, will take ' num2str( nframes * T ) ' seconds' ] )
va.setTimer( T );
for n = 1:nframes
    va.waitForTimer;
    x_pos = -50 + ( n - 1 ) * T * vehicle_velocity;
    va.setSoundSourcePosition( car, [ x_pos 0.5 -3.6 ] );
end
disp( [ 'Loop end, cleaning up' ] )

%% Remove listeners to export WAV files
%va.setActiveListener( -1 )
va.deleteListener( L1 )
va.deleteListener( L2 )
va.deleteListener( L3 )
va.deleteListener( L4 )

%%
va.reset
va.disconnect