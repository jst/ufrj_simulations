%% Preps resources
sine_continuous = ita_generate( 'sine', 1.0, 340.0, 44100 ,15, 'fullperiod' );
ita_write_wav( sine_continuous, '../va/data/sine_continuous.wav', 'overwrite' );


%% Preps VA
va = itaVA( 'localhost' );
va.reset();
va.addSearchPath( pwd ); 
va.addSearchPath( 'va/data' );

% Define a special target folder for WAV files
simulation_base_path = fullfile( pwd, datestr( now, 'YYYY-mm-dd_HH-MM' ) );
[~,~,~] = mkdir( simulation_base_path );
va.callModule( 'MonauralFreeField:UFRJ', struct( 'basefolder', simulation_base_path ) );


%% VA scene
X = va.createAudiofileSignalSource( 'sine_continuous.wav' );
va.setAudiofileSignalSourcePlaybackAction( X, 'play' );
va.setAudiofileSignalSourceIsLooping( X, true );

S = va.createSoundSource( 'car' );
va.setSoundSourcePosition( S, [ 0 1.2 -3.6 ] );
va.setSoundSourceSignalSource( S, X );

H = va.loadHRIRDataset( '$(DefaultHRIR)' );
L = va.createListener( 'array_center', 'all', H );
va.setListenerPosition( L, [ 0 1.2 0 ] );


%% Run as long as required
pause( 2.0 )


%% Store data
va.deleteSoundSource( S );
va.deleteListener( L );
va.reset();
